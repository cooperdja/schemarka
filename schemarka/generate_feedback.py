#!/usr/bin/python

from .schema import Schema, Section, Criterion, Result, Feedback, schemaFile
from .schema_loaders import xml_schema_loader

import argparse
import os
import re
import textwrap
import datetime

def sectionHtml(section: Section, result: Result) -> str:
    htmlEntries = []
    for entry in section.entries:
        if isinstance(entry, Section):
            htmlEntries.append(
                f'<tr><td colspan="2" class="secheading">{entry.name}</td></tr>{sectionHtml(entry, result)}')

        elif isinstance(entry, Criterion):
            htmlEntries.append(f'''
                <tr>
                    <td>{entry.name}</td>
                    <td>
                        <span class="mark">{result.marks[entry]}</span>
                        <span class="outof">/ {entry.nMarks}</span>
                    </td>
                </tr>''')

        elif isinstance(entry, Feedback):
            feedback = (result.feedback[entry] or '').strip()
            if feedback:
                htmlEntries.append(f'''
                    <tr>
                        <td class="feedback">{feedback}</span>
                        <td></td>
                    </tr>''')

        else:
            pass # Do nothing with 'record' entries; they are private.


    return ''.join(htmlEntries)


def generateHtmlFeedback(s: Schema, result: Result) -> str:

    totalHtml = f'''<tr>
        <td class="total">Total</td>
        <td class="total">
            <span class="mark">{result.totalMark}</span>
            <span class="outof">/ {s.totalMarks}</span>
        </td>
    '''

    if result.latePenalty is not None and result.latePenalty > 0:
        totalHtml = f'''
            <tr>
                <td class="latepenalty">Late Penalty</td>
                <td class="latepenalty">&minus;{result.latePenalty}</td>
            </tr>
            {totalHtml}'''

    return textwrap.dedent(f'''
        <!DOCTYPE html>
        <html>
        <head>
            <title{s.unitAbbrev}: {s.assessment}</title>
            <style>
                html {{
                    font-family: serif;
                    line-height: 1.8em;
                    background: #404040;
                }}

                body {{
                    background: white;
                    max-width: 50em;
                    margin-left: auto;
                    margin-right: auto;
                    padding: 4em;
                }}

                h1, h2, h3 {{
                    font-family: sans-serif;
                }}

                .box {{
                    border-radius: 5px;
                    border: 1px solid #606060;
                    padding: 0 1em;
                    margin: 1ex 0;
                }}

                table {{
                    border-collapse: collapse;
                }}

                td {{
                     padding-left: 1em;
                     padding-right: 1em;
                     padding-bottom: 0.5em;
                }}

                td:nth-child(2) {{
                    text-align: center;
                    white-space: nowrap;
                }}

                tr:hover {{
                    background: #c0d8ff;
                }}

                .secheading {{
                    font-weight: bold;
                    font-family: sans-serif;
                    border-top: 1px solid #606060;
                }}

                .outof {{
                    color: #808080;
                    font-size: small;
                }}

                .feedback {{
                    font-style: italic;
                    padding-left: 3em;
                    white-space: pre-line;
                }}

                .latepenalty {{
                    background: #c0c0c0;
                }}

                .total {{
                    font-weight: bold;
                    background: black;
                    color: white;
                }}

            </style>
        </head>
        <body>
            <h1>{s.unitAbbrev} {s.assessment}: Marks</h1>
            <div class="box">
                <p>
                    <strong>{s.unit}</strong><br/>
                    <strong>{s.studyPeriod}</strong><br/>
                    <em>Generated at {datetime.datetime.now().isoformat(' ', timespec='minutes')}</em>
                </p>

                <p><strong>{result.lastName}, {result.firstName} ({result.id})</strong></p>
            </div>

            <table>
                <thead>
                    <tr><td></td><th>Mark</th></tr>
                </thead>
                <tbody>
                    {sectionHtml(s.criteria, result)}
                    {totalHtml}
                </tbody>
            </table>
        </body>
        </html>''')


NORMAL = '\033[0m'
RED = '\033[31m'
GREEN = '\033[32m'
YELLOW = '\033[33m'
BRIGHT_RED = '\033[31;1m'


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('schemaFile', metavar='schema_file[.xml]', type=schemaFile);
    #parser.add_argument('idList', metavar='ID', action='append', type=int, nargs='*')
    #parser.add_argument('-n', dest='name', nargs=1)

    args = parser.parse_args()
    s = xml_schema_loader.loadSchema(args.schemaFile)

    baseName = args.schemaFile.removesuffix('.xml').removesuffix('.schema')

    nWithErrors = 0
    nNonSubmissions = 0
    nSuppressed = 0
    nGenerated = 0

    for result in s.loadResults():
        studentStr = f'{result.lastName}, {result.firstName} ({result.id})'

        if result.errors:
            print(f'{BRIGHT_RED}!!! Skipping {studentStr} due to errors:{NORMAL}')
            for error in result.errors:
                print(f'    - {error}')
            print()
            nWithErrors += 1

        elif not result.submitted:
            print(f'{YELLOW}* Skipping due to non-submission: {studentStr}{NORMAL}')
            nNonSubmissions += 1

        elif not result.release:
            print(f'{RED}* Skipping due to release suppression: {studentStr}{NORMAL}')
            nSuppressed += 1

        else:
            print(f'{GREEN}* Generating feedback: {studentStr}{NORMAL}')
            with open(f'{baseName}_{result.id}_Marks.html', 'w') as writer:
                writer.write(generateHtmlFeedback(s, result))
            nGenerated += 1

    print(f'\n# with errors: {nWithErrors}\n# non-submissions: {nNonSubmissions}\n# suppressed: {nSuppressed}\n# feedback generated: {nGenerated}')


if __name__ == '__main__':
    main()
