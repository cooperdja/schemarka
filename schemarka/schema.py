import openpyxl

from datetime import datetime, timedelta
from decimal import Decimal
from dataclasses import dataclass, field
from enum import Enum, auto
import math
import os


class SchemaError(Exception): pass
class SpreadsheetError(Exception): pass


def schemaFile(s: str) -> str:
    if os.path.isfile(s):
        return s

    # If it's a spreadsheet, remove the existing extension
    parts = s.split('.')
    if len(parts) > 1 and parts[-1].lower() in ('xlsx', 'xls', 'ods'):
        s = '.'.join(parts[:-1])

    # In either case, try it again with '.xml' on the end
    s = s + '.xml'
    if os.path.isfile(s):
        return s

    raise ValueError('Schema file not found')




PRESERVE_DECIMALS = 10
def toDecimal(n) -> Decimal:
    d = round(Decimal(n), PRESERVE_DECIMALS)
    return d.quantize(1) if d == d.to_integral_value() else d.normalize()


class Student(Enum):
    id = auto()
    lastName = auto()
    firstName = auto()
    submitted = auto()
    submissionTime = auto()
    extensionDays = auto()
    lateDays = auto()
    rawTotalMark = auto()
    totalMark = auto()
    latePenalty = auto()
    release = auto()


class ColumnSpec:
    def __init__(self, i0: int):
        self.i0 = i0

    @property
    def i1(self):
        return self.i0 + 1

    @property
    def l(self):
        chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
        N = len(chars)

        name = chars[self.i0 % N]
        j = self.i0 // N
        while j > 0:
            j -= 1
            name = chars[j % N] + name
            j //= N

        return name

    def __str__(self):
        return self.l

    def __repr__(self):
        return self.l


@dataclass(frozen=True)
class Entry:
    label: str


@dataclass(frozen=True,repr=False)
class ColumnEntry(Entry):
    column: ColumnSpec

    @property
    def firstColumn(self): return self.column

    @property
    def lastColumn(self): return self.column

    def __repr__(self):
        return self.label


@dataclass(frozen=True,repr=False)
class Criterion(ColumnEntry):
    name: str
    nMarks: Decimal
    maxMark: Decimal
    minMark: Decimal
    incMark: Decimal = Decimal('0.5')


@dataclass(frozen=True)
class Record(ColumnEntry):
    pass


@dataclass(frozen=True)
class Feedback(ColumnEntry):
    name: str


@dataclass(frozen=True)
class Section(Entry):
    name: str
    entries: list[Entry] = field(default_factory=list)

    @property
    def firstColumn(self):
        return self.entries[0].firstColumn if self.entries else None

    @property
    def lastColumn(self):
        return self.entries[-1].lastColumn if self.entries else None


@dataclass
class Result:
    id: int
    lastName: str
    firstName: str
    submitted: bool
    submissionTime: datetime
    extensionDays: int
    lateDays: int
    rawTotalMark: Decimal
    latePenalty: Decimal
    totalMark: Decimal
    release: bool
    marks: dict[Criterion, Decimal] = field(default_factory=dict)
    feedback: dict[Feedback, str] = field(default_factory=dict)
    records: dict[Record, object] = field(default_factory=dict)
    errors: list[str] = field(default_factory=list)


@dataclass
class Schema:
    file: str
    sheet: str
    unit: str
    unitAbbrev: str
    assessment: str
    studyPeriod: str
    totalMarks: Decimal
    dueTime: datetime
    graceMinutes: int
    freeze: ColumnSpec = None
    student: dict[Student, ColumnSpec] = field(default_factory = dict)
    criteria: Section = field(default_factory = lambda: Section(label='Criteria', name='Criteria'))


    def loadResults(self, spreadsheetFile: str = None, worksheetId = None) -> list[Result]:

        def loadSection(result: Result, section: Section, row, malformedErrors, missingErrors):
            for entry in section.entries:
                if isinstance(entry, Section):
                    # Recurse
                    loadSection(result, entry, row, malformedErrors, missingErrors)

                elif isinstance(entry, Criterion):
                    # Read actual mark!
                    value = row[entry.column.i0].value
                    if isinstance(value, int) or isinstance(value, float):
                        result.marks[entry] = toDecimal(value)

                    else:
                        result.marks[entry] = None
                        if value is None:
                            missingErrors.append(
                                f'Missing value for criterion {entry} in column {entry.column}')

                        else:
                            malformedErrors.append(
                                f'Non-numeric value "{value}" for criterion "{entry.label}" in column {entry.column}')

                elif isinstance(entry, Record):
                    result.records[entry] = row[entry.column.i0].value

                elif isinstance(entry, Feedback):
                    value = row[entry.column.i0].value
                    if value is None or isinstance(value, str):
                        result.feedback[entry] = value
                        # Missing feedback (None) isn't an error

                    else:
                        malformedErrors.append(
                            f'Non-textual value {repr(value)} in feedback column {entry.column}')

                else:
                    raise AssertionError


        def optionalColumn(row, field, errorList, converter = str, default = None):
            try:
                value = row[self.student[field].i0].value
            except IndexError:
                value = None
                errorList.append(
                    f'Spreadsheet does not contain column {self.student[field]} for {field.name}')

            if value is not None:
                try:
                    value = converter(value)
                except Exception:
                    value = None

                if value is not None: # Might also be set to None by the converter
                    return value

                errorList.append(
                    f'Invalid value {repr(value)} for {field.name} in column {self.student[field]}')

            return default


        def requiredColumn(row, field, errorList, converter = str):
            errors = len(errorList)
            value = optionalColumn(row, field, errorList, converter)

            if value is None and errors == len(errorList):
                errorList.append(
                    f'Missing value for {field.name} in column {self.student[field]}')

            return value



        results = []

        spreadsheetFile = spreadsheetFile or self.file
        wId = worksheetId or self.sheet
        try: wId = int(wId)
        except ValueError: pass

        workbook = openpyxl.load_workbook(spreadsheetFile, data_only=True)
        worksheet = workbook.worksheets[wId] if isinstance(wId, int) else workbook[wId]

        for row in worksheet.iter_rows():
            id = row[self.student[Student.id].i0].value
            if isinstance(id, int):
                malformedErrors = []
                missingErrors = []
                result = Result(
                    id = id,
                    lastName =       requiredColumn(row, Student.lastName,       malformedErrors, str),
                    firstName =      requiredColumn(row, Student.firstName,      malformedErrors, str),
                    submitted =      optionalColumn(row, Student.submitted,      malformedErrors, bool, False),
                    submissionTime = optionalColumn(row, Student.submissionTime, missingErrors, lambda v: v if isinstance(v, datetime) else None),
                    extensionDays =  optionalColumn(row, Student.extensionDays,  missingErrors, int),
                    lateDays =       optionalColumn(row, Student.lateDays,       missingErrors, int, 0),
                    rawTotalMark =   optionalColumn(row, Student.rawTotalMark,   missingErrors, toDecimal),
                    latePenalty =    optionalColumn(row, Student.latePenalty,    missingErrors, toDecimal, Decimal(0)),
                    totalMark =      optionalColumn(row, Student.totalMark,      missingErrors, toDecimal),
                    release =        optionalColumn(row, Student.release,        malformedErrors, bool, True)
                )
                results.append(result)
                loadSection(result, self.criteria, row, malformedErrors, missingErrors)

                result.errors.extend(malformedErrors)

                if result.submitted:

                    # Complain about "missing" errors only in case of submission
                    result.errors.extend(missingErrors)

                    # Check marks are within bounds
                    for criterion, mark in result.marks.items():
                        # Note: we've already checked for None values above.
                        if mark is not None:
                            #if mark is None:
                                #result.errors.append(
                                    #f'Missing mark in column {criterion.column} ("{criterion.label}")')

                            if mark < criterion.minMark:
                                result.errors.append(
                                    f'Invalid mark {mark} (< {criterion.minMark}) in column {criterion.column} ("{criterion.label}")')

                            elif mark > criterion.maxMark:
                                result.errors.append(
                                    f'Invalid mark {mark} (> {criterion.maxMark}) in column {criterion.column} ("{criterion.label}")')

                            if mark % criterion.incMark != 0:
                                result.errors.append(
                                    f'Invalid mark {mark} (not divisible by {criterion.incMark}) in column {criterion.column} ("{criterion.label}")')

                    # Check adding up
                    calculatedRawTotalMark = max(0, min(self.totalMarks, sum(mark or 0 for mark in result.marks.values())))
                    if calculatedRawTotalMark != result.rawTotalMark:
                        result.errors.append(
                            f'Sum of marks ({calculatedRawTotalMark}) does not match specified raw total ({result.rawTotalMark}) in column {self.student[Student.rawTotalMark]}')

                    else:
                        calculatedLatePenalty = 0
                        calculatedTotalMark = calculatedRawTotalMark
                        if result.submissionTime:
                            # Late penalty calculation, if we know the submission time

                            submissionDeadline = self.dueTime + timedelta(minutes=self.graceMinutes)
                            if result.extensionDays:
                                submissionDeadline += timedelta(days=result.extensionDays)

                            secondsDelta = (result.submissionTime - submissionDeadline).total_seconds()
                            lateDays = max(0, math.ceil(secondsDelta / 86400))

                            if lateDays != result.lateDays:
                                result.errors.append(
                                    f'Calculated late days ({lateDays}) does not match specified late days ({result.lateDays}) in column {self.student[Student.lateDays]}')

                            if lateDays > 0:
                                if lateDays > 7:
                                    calculatedLatePenalty = self.totalMarks
                                else:
                                    calculatedLatePenalty = min(
                                        self.totalMarks,
                                        self.totalMarks * (Decimal(lateDays) * Decimal('0.1') - Decimal('0.05')))
                                calculatedTotalMark = max(0, calculatedRawTotalMark - calculatedLatePenalty)

                        if calculatedLatePenalty != result.latePenalty:
                            result.errors.append(
                                f'Calculated late penalty ({calculatedLatePenalty}) does not match specified late penalty ({result.latePenalty}) in column {self.student[Student.latePenalty]}')

                        elif calculatedTotalMark != result.totalMark:
                            result.errors.append(
                                f'Calculated total mark ({calculatedTotalMark}) does not match specified total ({result.totalMark}) in column {self.student[Student.totalMark]}')

                else:
                    # Non-submission: verify that there have *not*, in fact, been any marks recorded
                    allMarks = [*result.marks.values(), result.rawTotalMark, result.totalMark]
                    if not all(mark is None or isinstance(mark, str) for mark in allMarks):

                        result.errors.append(
                            f'Mark(s) recorded despite non-submission: {[str(mark) for mark in allMarks if mark is not None and not isinstance(mark, str)]}')

                    if result.submissionTime is not None:
                        result.errors.append(
                            f'Submission time recorded despite non-submission')


        return results



