#!/usr/bin/python

from .schema import Schema, Criterion, Section, ColumnSpec, Student, Record, Feedback, SchemaError, schemaFile
from .schema_loaders import xml_schema_loader

import openpyxl
import csv
import zipfile
import xml.etree.ElementTree as ElementTree

import argparse
import os
import re
from datetime import timedelta
import itertools
from typing import List, Tuple, Union, Optional


# For reference, these forms occur "in the wild":
#
# Blackboard Offline:                                       "Last Name", "First Name", "Username", "Student ID"
# eAcademic -> Current Students -> Export selected records: "Student Id", "Family Name", "Preferred Given Name"
# eAcademic -> Current Students -> Export all:              "StuID", "StuFamilyNm", "StuGivenNm"

ID_HEADING = re.compile('((stu|student)?id)|username')
LAST_NAME_HEADING = re.compile('(stu|student)?(last|family)(name|nm)')
FIRST_NAME_HEADING = re.compile('(stu|student|pref|preferred)?(first|given)(name|nm)')

class SpreadsheetError(Exception): pass
class StudentListError(Exception): pass
class SheetCreationError(Exception): pass


# Mimicks small parts of openpyxl's interface, but for CSV/ODS files.
class Worksheet:
    class Cell:
        def __init__(self, value: Optional[str]):
            try:
                self.value = None if value is None else int(value)
            except ValueError:
                self.value = value

    EMPTY_CELL = Cell(None)

    def __init__(self):
        self._data = []
        self._width = None


    @staticmethod
    def loadCsv(filename: str):
        sheet = Worksheet()
        try:
            # Note: 'utf-8-sig' will strip the BOM (byte order mark) from the start of the stream.
            for enc in ['utf-8-sig', 'utf-16', 'utf-32']:
                try:
                    with open(filename, newline='', encoding=enc) as fileReader:
                        sheet._data = [
                            [Worksheet.Cell(value) for value in csvRow]
                            for csvRow in csv.reader(fileReader)
                        ]
                        return sheet

                except UnicodeDecodeError:
                    pass # Try next encoding

            # Out of encodings
            raise SpreadsheetError(f'Unknown file encoding in {filename}')

        except IOError as e:
            raise SpreadsheetError from e


    @staticmethod
    def loadOds(filename: str, sheetId: Union[int,str]):
        sheet = Worksheet()

        NS = {
            name: f'urn:oasis:names:tc:opendocument:xmlns:{name}:1.0'
            for name in ['office', 'table', 'text']
        }

        N_COLS_REPEATED = '{{{NS["table"]}}}number-columns-repeated'

        try:
            with zipfile.ZipFile(filename) as zipReader:
                tree = ElementTree.parse(zipReader.open('content.xml'))

                xpathCondition = (sheetId + 1) if isinstance(sheetId, int) else f'[@table:name="{sheetId}"]'
                table = tree.find(f'.//table:table[{xpathCondition}]', NS)
                if table is None:
                    raise SpreadsheetError(f'No such sheet {sheetId} in {filename}')


                for xmlRow in table.iterfind('.//table:table-row', NS):
                    row = []
                    sheet._data.append(row)

                    for xmlCell in xmlRow.iterfind('*', NS):
                        text = xmlCell.findtext('*', '')
                        for i in range(int(xmlCell.get(N_COLS_REPEATED, '1'))):
                            row.append(Worksheet.Cell(text))

                return sheet

        except (IOError, zipfile.BadZipFile, zipfile.LargeZipFile, ElementTree.ParseError) as e:
            raise SpreadsheetError from e


    @property
    def width(self):
        if self._width is None:
            self._width = max(len(row) for row in self._data)
        return self._width


    def iter_rows(self):
        for row in self._data:
            yield row + [self.EMPTY_CELL] * (self.width - len(row))

    def iter_cols(self):
        return itertools.zip_longest(*self._data, fillvalue=self.EMPTY_CELL)



def findColumn(worksheet, headingPattern: re.Pattern) -> int:
    for colN, col in enumerate(worksheet.iter_cols()):
        nonBlankRowN = 0
        for cell in col:
            if cell.value is None:
                continue        # Ignore blank rows

            if nonBlankRowN > 2 or not isinstance(cell.value, str):
                break           # Check next column

            if headingPattern.fullmatch(cell.value.lower().replace(' ', '')):
                return colN     # Column found!

            nonBlankRowN += 1   # Check next row

    return None # No matching column


def findIdColumn(worksheet) -> int:
    # Backup in case we can't find the ID column by heading; find the first column containing an eight-digit integer:
    for colN, col in enumerate(worksheet.iter_cols()):
        for cell in col:
            if isinstance(cell.value, int):
                if len(str(cell.value)) == 8:
                    return colN
    return None


def validateName(value, label, id):
    if value is None:
        raise StudentListError(f'Missing {label} for id {id}')

    if not isinstance(value, str):
        raise StudentListError(f'Non-textual value {repr(value)} in {label} column for id {id}')


def readStudentList(spreadsheetFile: str, sheetId: Union[int,str] = 0) -> tuple[int,str,str]:

    lc = spreadsheetFile.lower()

    if lc.endswith('.xlsx'):
        workbook = openpyxl.load_workbook(spreadsheetFile, data_only=True)
        worksheet = workbook.worksheets[sheetId] if isinstance(sheetId, int) else workbook[sheetId]

    elif lc.endswith('.csv'):
        worksheet = Worksheet.loadCsv(spreadsheetFile)

    elif lc.endswith('.ods'):
        worksheet = Worksheet.loadOds(spreadsheetFile, sheetId)

    else:
        raise StudentListError(f'Unsupported student list file format in {spreadsheetFile}')

    idColumn = findColumn(worksheet, ID_HEADING) or findIdColumn(worksheet)
    lastNameColumn = findColumn(worksheet, LAST_NAME_HEADING)
    firstNameColumn = findColumn(worksheet, FIRST_NAME_HEADING)

    if idColumn is None:
        raise StudentListError(f'Cannot identify a student-ID column in {spreadsheetFile}, sheet {sheetId}')

    if lastNameColumn is None:
        raise StudentListError(f'Cannot identify a last/family-name column in {spreadsheetFile}, sheet {sheetId}')

    if firstNameColumn is None:
        raise StudentListError(f'Cannot identify a first/given-name column in {spreadsheetFile}, sheet {sheetId}')


    studentList = []
    for row in worksheet.iter_rows():
        id = row[idColumn].value
        if isinstance(id, int):
            lastName = row[lastNameColumn].value
            firstName = row[firstNameColumn].value
            validateName(lastName, 'last/family name', id)
            validateName(firstName, 'first/given name', id)
            studentList.append((id, lastName, firstName))

    return studentList



STUDENT_COLUMN_NAMES = {
    Student.id:             'Student ID',
    Student.lastName:       'Last Name',
    Student.firstName:      'First Name',
    Student.submitted:      'Submitted (TRUE/FALSE)',
    Student.rawTotalMark:   'Raw Total',
    Student.submissionTime: 'Submission Time',
    Student.extensionDays:  'Extension (days)',
    Student.lateDays:       'Late (days)',
    Student.latePenalty:    'Late Penalty',
    Student.totalMark:      'Total',
    Student.release:        'Release (TRUE/FALSE)'
}

CM_TO_EXCEL_DIMS = 1.0 / 0.196

STUDENT_COLUMN_WIDTHS = {
    Student.id:             2.05,
    Student.lastName:       3.00,
    Student.firstName:      3.00,
    Student.submitted:      2.02,
    Student.rawTotalMark:   2.0,
    Student.submissionTime: 3.60,
    Student.extensionDays:  2.07,
    Student.lateDays:       1.2,
    Student.latePenalty:    1.53,
    Student.totalMark:      1.11,
    Student.release:        2.02,
}

ENTRY_COLUMN_WIDTHS = {
    Criterion: 1.1,
    Record:    3.0,
    Feedback:  5.0
}

CENTERED_COLUMNS = {
    Student.id,
    Student.submitted,
    Student.rawTotalMark,
    Student.submissionTime,
    Student.extensionDays,
    Student.lateDays,
    Student.latePenalty,
    Student.totalMark,
    Student.release,
}

HEADING_FONT = openpyxl.styles.Font(name='Arial', size=10, bold=True)
DATA_FONT    = openpyxl.styles.Font(name='Arial', size=10)

CENTERED_HEADING = openpyxl.styles.Alignment(horizontal='center',  wrap_text=True)
LEFT_HEADING     = openpyxl.styles.Alignment(horizontal='general', wrap_text=True)

CENTERED_DATA  = openpyxl.styles.Alignment(horizontal='center', vertical='center')
LEFT_DATA      = openpyxl.styles.Alignment(horizontal='general', vertical='center')
MULTILINE_DATA = openpyxl.styles.Alignment(horizontal='general', vertical='center', wrap_text=True)

ERROR_FILL = openpyxl.styles.PatternFill(bgColor='ffc7ce')

def putEntryHeadings(worksheet, section: Section, topRowN: int, bottomRowN: int):
    assert topRowN < bottomRowN
    for entry in section.entries:
        if isinstance(entry, Section):
            if entry.firstColumn is not None:
                cell = worksheet.cell(topRowN, entry.firstColumn.i1, entry.label)
                cell.font = HEADING_FONT
                worksheet.merge_cells(start_row=topRowN,
                                      end_row=topRowN,
                                      start_column=entry.firstColumn.i1,
                                      end_column=entry.lastColumn.i1)
            putEntryHeadings(worksheet, entry, topRowN + 1, bottomRowN)


        else:
            cell = worksheet.cell(topRowN, entry.column.i1, entry.label)
            cell.font = HEADING_FONT
            worksheet.column_dimensions[entry.column.l].width = ENTRY_COLUMN_WIDTHS[type(entry)] * CM_TO_EXCEL_DIMS

            if bottomRowN - topRowN >= 2:
                worksheet.merge_cells(start_row=topRowN,
                                      end_row=bottomRowN - 1,
                                      start_column=entry.column.i1,
                                      end_column=entry.column.i1)

            if isinstance(entry, Criterion):
                cell.alignment = CENTERED_HEADING

                if entry.minMark == 0 and entry.maxMark == entry.nMarks:
                    markSpec = f'(/{entry.nMarks})'

                elif entry.minMark == 0:
                    markSpec = f'(/{entry.nMarks}, +{entry.maxMark - entry.nMarks})'

                elif entry.maxMark == entry.nMarks:
                    markSpec = f'({entry.minMark}..{entry.maxMark})'

                else:
                    markSpec = f'({entry.minMark}..{entry.nMarks}, +{entry.maxMark - entry.nMarks})'

                cell = worksheet.cell(bottomRowN, entry.column.i1, markSpec)
                cell.font = HEADING_FONT
                cell.alignment = CENTERED_HEADING

            else:
                cell.alignment = LEFT_HEADING


def getSectionTreeDepth(section: Section):
    subTreeDepths = [getSectionTreeDepth(entry)
                     for entry in section.entries if isinstance(entry, Section)]
    return 1 + max(subTreeDepths) if subTreeDepths else 1


def setRowStyles(worksheet, s: Schema, rowN: int):

    def forSection(section: Section):
        for entry in section.entries:
            if isinstance(entry, Section):
                forSection(entry)

            else:
                cell = worksheet.cell(rowN, entry.column.i1)
                cell.alignment = {
                    Criterion: CENTERED_DATA,
                    Feedback:  MULTILINE_DATA,
                    Record:    LEFT_DATA
                }[type(entry)]
                cell.font = DATA_FONT

                if isinstance(entry, Criterion):
                    # Set conditional formatting to highlight out-of-range values
                    worksheet.conditional_formatting.add(
                        cell.coordinate,
                        openpyxl.formatting.rule.CellIsRule(
                            operator = 'lessThan',
                            formula = [f'{entry.minMark}'],
                            stopIfTrue = True,
                            fill = ERROR_FILL))

                    worksheet.conditional_formatting.add(
                        cell.coordinate,
                        openpyxl.formatting.rule.CellIsRule(
                            operator = 'greaterThan',
                            formula = [f'{entry.maxMark}'],
                            stopIfTrue = True,
                            fill = ERROR_FILL))

    # Set styles for overview information
    for field, column in s.student.items():
        cell = worksheet.cell(rowN, column.i1)
        cell.alignment = CENTERED_DATA if field in CENTERED_COLUMNS else LEFT_DATA
        cell.font = DATA_FONT

    # Set styles for mark entry and related fields
    forSection(s.criteria)



def cellRef(column: ColumnSpec, rowN: int) -> str:
    return f'{column.l}{rowN}'


def makeCellList(worksheet, section: Section, rowN: int) -> list[str]:
    cellList = []
    for entry in section.entries:
        if isinstance(entry, Section):
            cellList.extend(makeCellList(worksheet, entry, rowN))

        elif isinstance(entry, Criterion):
            cellList.append(cellRef(entry.column, rowN))

    return cellList


def makeCellValidationList(worksheet, section: Section, rowN: int) -> list[str]:
    validationList = []
    for entry in section.entries:
        if isinstance(entry, Section):
            validationList.extend(makeCellValidationList(worksheet, entry, rowN))

        elif isinstance(entry, Criterion):
            ref = cellRef(entry.column, rowN)
            validationList.append(f'ISNUMBER({ref})')
            validationList.append(f'{ref} >= {entry.minMark}')
            validationList.append(f'{ref} <= {entry.maxMark}')

    return validationList


def populateRow(worksheet, s: Schema, rowN: int, dueTimeCellCoord):
    setRowStyles(worksheet, s, rowN)

    # Create summation formula
    submittedRef = cellRef(s.student[Student.submitted], rowN)
    cells = ','.join(makeCellList(worksheet, s.criteria, rowN))
    validation = ','.join(makeCellValidationList(worksheet, s.criteria, rowN))

    rawTotalCell = worksheet.cell(
        rowN, s.student[Student.rawTotalMark].i1,
        re.sub('\s+', ' ', f'''=
            IF(
                {submittedRef},
                IF(
                    COUNT({cells}) = 0,
                    "[not marked]",
                    IF(
                        AND({validation}),
                        MIN(MAX(SUM({cells}), 0), {s.totalMarks}),
                        MIN(MAX(SUM({cells}), 0), {s.totalMarks}) & " [!!]"
                    )
                ),
                IF(
                    COUNT({cells}) = 0,
                    "",
                    MIN(MAX(SUM({cells}), 0), {s.totalMarks}) & " [??]"
                )
            )
        ''')
    )

    # Create late penalty calculation
    submissionTimeCell = worksheet.cell(rowN, s.student[Student.submissionTime].i1)
    extensionCell =      worksheet.cell(rowN, s.student[Student.extensionDays].i1)

    lateDaysCell = worksheet.cell(rowN, s.student[Student.lateDays].i1,
        re.sub('\s+', ' ', f'''=
            IF(
                AND(ISBLANK({submissionTimeCell.coordinate}),ISBLANK({extensionCell.coordinate})),
                "",
                MAX(
                    0,
                    ROUNDUP({submissionTimeCell.coordinate} - {dueTimeCellCoord} - {extensionCell.coordinate})
                )
            )
        ''')
    )

    latePenaltyCell = worksheet.cell(rowN, s.student[Student.latePenalty].i1,
        re.sub('\s+', ' ', f'''=
            IF(
                {lateDaysCell.coordinate} = "",
                "",
                IF(
                    {lateDaysCell.coordinate} > 7,
                    {s.totalMarks},
                    MAX(0, {s.totalMarks} * ({lateDaysCell.coordinate} * 0.1 - 0.05))
                )
            )
        ''')
    )

    totalCell = worksheet.cell(rowN, s.student[Student.totalMark].i1,
        re.sub('\s+', ' ', f'''=
            IF(
                {rawTotalCell.coordinate} = "",
                "",
                IF(
                    ISNUMBER({rawTotalCell.coordinate}),
                    MAX(0, {rawTotalCell.coordinate} - N({latePenaltyCell.coordinate})),
                    "[!!]"
                )
            )
        ''')
    )

    # Highlight errors
    for cell in [rawTotalCell, totalCell]:
        worksheet.conditional_formatting.add(
            cell.coordinate,
            openpyxl.formatting.rule.FormulaRule(
                formula = [f'AND({cell.coordinate}<>"", NOT(ISNUMBER({cell.coordinate})))'],
                stopIfTrue = True,
                fill = ERROR_FILL))


def createMarkingSheet(spreadsheetFile: str,
                       s: Schema,
                       studentList: List[Tuple[int,str,str]],
                       extraRows: int):

    workbook = openpyxl.Workbook()
    worksheet = workbook.active

    worksheetTitle = s.sheet
    try:
        sheetN = int(s.sheet)
    except ValueError:
        # Interpret the sheet spec as a name, and set it accordingly
        worksheet.title = s.sheet
    else:
        # Interpret the sheet spec as an index, in which case 0 is the only valid one.
        if sheetN > 0:
            raise SheetCreationError(f'Cannot create a sheet with index {sheetN}')
        worksheet.title = 'Marks'

    nHeadingRows = getSectionTreeDepth(s.criteria)

    # Show the standard set of columns
    for field, column in s.student.items():
        headingCell = worksheet.cell(1, column.i1, STUDENT_COLUMN_NAMES[field])
        headingCell.font = HEADING_FONT
        headingCell.alignment = CENTERED_HEADING if field in CENTERED_COLUMNS else LEFT_HEADING
        worksheet.column_dimensions[column.l].width = STUDENT_COLUMN_WIDTHS[field] * CM_TO_EXCEL_DIMS
        if nHeadingRows > 1:
            worksheet.merge_cells(start_row=1,
                                  end_row=nHeadingRows,
                                  start_column=column.i1,
                                  end_column=column.i1)

        # Set the style of the other heading cells in the same column:
        for rowN in range(2, nHeadingRows + 2):
            cell = worksheet.cell(rowN, column.i1)
            cell.font = HEADING_FONT
            cell.alignment = CENTERED_HEADING if field in CENTERED_COLUMNS else LEFT_HEADING

    # Show totals available marks under the Raw Total" and "Total" headings.
    worksheet.cell(nHeadingRows + 1, s.student[Student.rawTotalMark].i1, f'(/{s.totalMarks})')
    worksheet.cell(nHeadingRows + 1, s.student[Student.totalMark].i1, f'(/{s.totalMarks})')

    # Show due time + grace period under "Submission Time" (not just for show; this is used in late penalty calculations).
    dueTimeCell = worksheet.cell(nHeadingRows + 1, s.student[Student.submissionTime].i1,
        s.dueTime + timedelta(minutes=s.graceMinutes))

    dueTimeCellCoord = openpyxl.utils.cell.absolute_coordinate(dueTimeCell.coordinate)

    # Increase heading size if only one row (to give enough room for the labels).
    if nHeadingRows == 1:
        worksheet.row_dimensions[1].height = 25

    # Show the assessment-specific column headings
    putEntryHeadings(worksheet, s.criteria, 1, nHeadingRows + 1)

    # Freeze panes
    worksheet.freeze_panes = cellRef(s.freeze or ColumnSpec(0), nHeadingRows + 2)

    # Paste in the student list and formulae
    rowN = nHeadingRows + 2
    for id, lastName, firstName in studentList:

        # Add student details:
        worksheet.cell(rowN, s.student[Student.id].i1,        id),
        worksheet.cell(rowN, s.student[Student.lastName].i1,  lastName),
        worksheet.cell(rowN, s.student[Student.firstName].i1, firstName)

        populateRow(worksheet, s, rowN, dueTimeCellCoord)
        rowN += 1

    for r in extraRows:
        populateRow(worksheet, s, rowN, dueTimeCellCoord)
        rowN += 1

    workbook.save(spreadsheetFile)


def studentListFile(s: str) -> str:
    if not re.match('.*\.(xlsx|ods|csv)', s, re.IGNORECASE) or not os.path.isfile(s):
        raise ValueError
    return s


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('schemaFile', metavar='schema_file[.xml]', type=schemaFile,
                        help='the schema XML file, indicating the column structure of the marking sheet')
    parser.add_argument('markingListFile', metavar='marking_sheet.xlsx', type=str, nargs='?',
                        help='the name of the marking sheet to create; if omitted, use the name given by the schema')
    parser.add_argument('-f', dest='force', action='store_true',
                        help='override destination if it already exists')
    parser.add_argument('-r', '--rows', nargs=1, metavar='N', type=int,
                        help='the number of blank data rows (empty of data, but with formulae in-place) to add')
    parser.add_argument('-l', '--list', nargs=1, metavar='student_list.[xlsx|csv]', type=studentListFile,
                        help='an input spreadsheet containing student IDs, last names and first names to help populate the output spreadsheet; these columns are detected based on several possible headings')
    #parser.add_argument('studentListFile', metavar='student_list.[xlsx|csv]', type=studentListFile,
                        #help='a student list spreadsheet, containing at a minimum student IDs, last names and first names; these columns are detected based on several possible headings');

    args = parser.parse_args()
    if args.rows is None and args.list is None:
        print('You must specify one (or both) of -l/--list or -r/--rows, to determine how the rows will be populated')
    else:
        try:
            studentList = readStudentList(args.list) if args.list else []
            extraRows = args.rows or 0

            s = xml_schema_loader.loadSchema(args.schemaFile)

            # Don't overwrite an existing spreadsheet, unless forced (-f).
            if os.path.exists(s.file) and not args.force:
                print(f'!!! Spreadsheet file {s.file} already exists; not overriding')

            else:
                createMarkingSheet(args.markingListFile or s.file, s, studentList, extraRows)

        except (SpreadsheetError, StudentListError) as e:
            print(f'!!! Problem loading student list: {e}')

        except SchemaError as e:
            print(f'!!! Problem loading schema: {e}')

        except SheetCreationError as e:
            print(f'!!! Problem creating new marking sheet: {e}')


if __name__ == '__main__':
    main()
